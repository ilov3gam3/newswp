<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <a href="admin/patient-control">doctor control</a>
    <h1>Views Patients</h1>
    <form action="LoadPatients" method="post">
        <input type="text" name="name" placeholder="name">
        <input type="email" name="email" placeholder="email">
        <input type="password" name="password" placeholder="password">
        <input type="tel" name="phone" placeholder="phone">
        <input type="date" name="dob">
        <select name="gender" id="gender">
            <option selected>Choose</option>
            <option value="0">nữ</option>
            <option value="1">nam</option>
        </select>
        <button type="submit">submit</button>
        <button type="reset">reset</button>
    </form>
    <br>
    <table style="border: 1px solid black">
        <tr>
            <th>index</th>
            <th>id</th>
            <th>name</th>
            <th>email</th>
            <th>password</th>
            <th>gender</th>
            <th>age</th>
            <th>action</th>
        </tr>
        <c:forEach items="${list}" var="x" varStatus="status">
            <tr>
                <td>${status.index + 1}</td>
                <td>${x.id}</td>
                <td>${x.name}</td>
                <td>${x.email}</td>
                <td>${x.password}</td>
                <td>${x.phone}</td>
                <td>${x.isGender() == false ? "Nữ" : "Nam"}</td>
                <td>${x.dob}</td>
                <td>
                    <a role="button" class="btn btn-info" href="update?pid=${x.id}">Update</a>
                    <a role="button" class="btn btn-danger" href="DeleteControl?pid=${x.id}">Delete</a>
                </td>
            </tr>
        </c:forEach>
    </table>

</body>
</html>