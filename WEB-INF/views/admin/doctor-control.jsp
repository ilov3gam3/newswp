<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        .table {
            width: 100%;
        }

        .table td {
            width: 100px; /* Adjust the width as needed */
            height: 100px; /* Adjust the height as needed */
        }

        .table img {
            width: 100%;
            height: 100%;
            object-fit: cover;
        }
    </style>
</head>
<body>
${message}
<form action="" method="post" enctype="multipart/form-data">
    <input type="hidden" name="_method" value="">
    <input type="text" name="name" placeholder="name">
    <input type="email" name="email" placeholder="email">
    <input type="password" name="password" placeholder="password">
    <input type="text" name="degree" placeholder="degree">
    <input type="number" name="experience" placeholder="experience">
    <select name="speciality_id" id="">
        <option value="0" selected>speciality_id</option>
        <c:forEach var="item" items="${speciality_list}">
            <option value="${item.id}">${item.getName()}</option>
        </c:forEach>
    </select>
    <input type="file" name="image">
    <input type="tel" name="phone" placeholder="phone">
    <input type="date" name="dob">
    <select name="gender">
        <option value="false">nữ</option>
        <option value="true">nam</option>
    </select>
    <input type="text" name="address" placeholder="address">
    <button>submit</button>
</form>
<br>
<table border="1">
    <tr>
        <th>id</th>
        <th>name</th>
        <th>email</th>
        <th>degree</th>
        <th>experience</th>
        <th>speciality_id</th>
        <th style="max-width: 700px">image</th>
        <th>phone</th>
        <th>gender</th>
        <th>dob</th>
        <th>address</th>
        <th>action</th>
    </tr>
    <c:forEach var="item" items="${doctor_list}">
        <tr>
            <td>${item.id}</td>
            <td>${item.getName()}</td>
            <td>${item.getEmail()}</td>
            <td>${item.getDegree()}</td>
            <td>${item.getExperience()}</td>
            <td>${item.getSpeciality_name()}</td>
            <td style="max-width: 700px"><img style="max-width: 100%; object-fit: cover" src="${item.getImage()}"
                                              alt=""></td>
            <td>${item.getPhone()}</td>
            <td>${item.isGender() == false ? "Nữ" : "Nam"}</td>
            <td>${item.getDob()}</td>
            <td>${item.getAddresses()}</td>
            <th>
                <form action="" method="post">
                    <input type="hidden" name="_method" value="DELETE">
                    <input type="hidden" name="id" value="${item.id}">
                    <button>delete</button>
                </form>
                <button>update</button>
            </th>
        </tr>
    </c:forEach>
</table>
</body>
</html>
