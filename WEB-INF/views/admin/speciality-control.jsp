<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
${message}
<form action="" method="post">
    <input type="hidden" name="_method" value="">
    <input type="text" name="name">
    <button>submit</button>
</form>
<table border="1">
    <tr>
        <th>id</th>
        <th>name</th>
        <th>action</th>
    </tr>
    <c:forEach var="item" items="${speciality_list}">
        <tr>
            <td>${item.id}</td>
            <td> ${item.getName()}</td>
            <td>
                <div style="display: flex" class="row">
                    <form action="" method="POST">
                        <input type="hidden" name="_method" value="DELETE">
                        <input type="hidden" value="${item.id}" name="id">
                        <button>delete</button>
                    </form>
                </div>

            </td>
        </tr>
    </c:forEach>
</table>
</body>
</html>