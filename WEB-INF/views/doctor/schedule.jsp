<%@ page contentType="text/html; charset=UTF-8" %>
<jsp:include page="../master/head.jsp"/>
<style>
    .custom-container {
        width: 80%;
        margin-left: auto;
        margin-right: auto;
        font-size: 16px;
    }
</style>
<div class="custom-container">
    <h1>Doctor schedule</h1>
    <p class="text-danger">${error}</p><br>
    <p class="text-success">${success}</p><br>
    <form action="" method="post">
        <input type="hidden" name="_method" value="get_date">
        <div class="row">
            <input value="${current_week}" required class="form-control m-2" name="week" style="width: 200px; height: 30px"
                   type="week">
            <button class="btn btn-warning">show</button>
        </div>
    </form>
    <table class="table table-bordered">
        <thead>
        <tr>
            <% String[][] table = (String[][]) request.getAttribute("table"); %>
            <% for (int i = 0; i < 8; i++) { %>
            <th scope="col"><%= table[0][i]%>
            </th>
            <% } %>
        </tr>
        </thead>
        <tbody>
            <% for (int i = 1; i < 11; i++) { %>
                <tr>
                    <% for (int j = 0; j < 8; j++) { %>
                    <% String style = ""; style= table[i][j] != null ? (table[i][j].equals("bận") ? "style='background-color: #FF515B'" : "") : ""; %>
                        <td <%=style%> ><%=table[i][j] == null ? "" : table[i][j]%></td>
                    <% } %>
                </tr>
            <% } %>
<%--        <tr>--%>
<%--            <td>8h-9h</td>--%>
<%--            <td>${date[0]}</td>--%>
<%--            <td>${date[1]}</td>--%>
<%--            <td>${date[2]}</td>--%>
<%--            <td>${date[3]}</td>--%>
<%--            <td>${date[4]}</td>--%>
<%--            <td>${date[5]}</td>--%>
<%--            <td>${date[6]}</td>--%>
<%--        </tr>--%>
<%--        <tr>--%>
<%--            <td>9h-10h</td>--%>
<%--            <td>${date[0]}</td>--%>
<%--            <td>${date[1]}</td>--%>
<%--            <td>${date[2]}</td>--%>
<%--            <td>${date[3]}</td>--%>
<%--            <td>${date[4]}</td>--%>
<%--            <td>${date[5]}</td>--%>
<%--            <td>${date[6]}</td>--%>
<%--        </tr>--%>
        <!-- Add more rows for each week -->
        </tbody>
    </table>
    <button class="btn btn-info" data-toggle="modal" data-target="#addModel">Thêm lịch</button>
    <div class="modal fade" id="addModel" tabindex="-1" role="dialog" aria-labelledby="addModel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="" method="post">
                    <input type="hidden" name="_method" value="add_schedule">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="custom-container form-group mb-1">
                            <label for="date">Ngày</label>
                            <input class="form-control" required type="date" name="date" id="date">
                        </div>
                        <div class="custom-container form-group mt-1 mb-1">
                            <label for="from">Từ (8-17)</label>
                            <input class="form-control" min="8" max="17" required type="number" name="from" id="from">
                        </div>
                        <div class="custom-container form-group mt-1">
                            <label for="to">Đến (8-17, phải lớn hơn thời gian bắt đầu)</label>
                            <input class="form-control" min="8" max="17" required type="number" name="to" id="to">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<jsp:include page="../master/foot.jsp"/>