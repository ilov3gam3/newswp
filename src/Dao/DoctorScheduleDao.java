package Dao;

import Contact.ContactDB;
import Model.DoctorSchedule;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class DoctorScheduleDao {
    Connection connection = null;
    PreparedStatement preparedStatement = null;
    ResultSet resultSet = null;
    public boolean addSchedule(int doctor_id,String date, int start, int end){
        String time_start = date + " " + start + ":00:00";
        String time_end = date + " " + end + ":00:00";
        String sql = "insert into doctor_schedule(doctor_id, start, end) values(?, ?, ?)";
        try {
            connection = ContactDB.makeConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, doctor_id);
            preparedStatement.setString(2, time_start);
            preparedStatement.setString(3, time_end);
            preparedStatement.execute();
            return true;
        } catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }
    public ArrayList<DoctorSchedule> getScheduleOfWeek(int doctor_id, String first_date_week, String last_date_week){
        ArrayList<DoctorSchedule> doctorScheduleArrayList = new ArrayList<DoctorSchedule>();
        first_date_week = first_date_week + " 07:00:00";
        last_date_week = last_date_week + " 18:00:00";
        String sql = "SELECT * from `doctor_schedule` where `doctor_id` = ? and `start` > ? and `end` < ?;";
        try {
            connection = ContactDB.makeConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1,doctor_id);
            preparedStatement.setString(2,first_date_week);
            preparedStatement.setString(3,last_date_week);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                doctorScheduleArrayList.add(new DoctorSchedule(resultSet.getInt("id"), resultSet.getInt("doctor_id"), resultSet.getString("start"), resultSet.getString("end")));
            }
            return doctorScheduleArrayList;
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
}
