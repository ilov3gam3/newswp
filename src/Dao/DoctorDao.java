package Dao;

import Contact.ContactDB;
import Model.Doctor;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class DoctorDao {
    Connection connection = null;
    PreparedStatement preparedStatement = null;
    ResultSet resultSet = null;
    public boolean createDoctor(String name,String email, String password, String degree, int experience, int speciality_id,String image, String phone, String dob, boolean gender, String address){
        String sql = "insert into doctors(name,email, password,degree,experience,speciality_id,image,phone, dob, gender,address ) values(?, ?, ?, ?, ?,?,?, ?, ?, ?, ?)";
        try {
            this.connection = ContactDB.makeConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, name);
            preparedStatement.setString(2, email);
            preparedStatement.setString(3, password);
            preparedStatement.setString(4, degree);
            preparedStatement.setInt(5, experience);
            preparedStatement.setInt(6, speciality_id);
            preparedStatement.setString(7, image);
            preparedStatement.setString(8, phone);
            preparedStatement.setString(9, dob);
            preparedStatement.setBoolean(10, gender);
            preparedStatement.setString(11, address);
            preparedStatement.execute();
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
        return true;
    }
    public ArrayList<Doctor> getAllDoctor() throws ClassNotFoundException, SQLException {
        ArrayList<Doctor> doctorArrayList = new ArrayList<>();
        String sql = "select doctors.*, speciality.name as speciality_name from doctors JOIN speciality ON doctors.speciality_id = speciality.id;";
        connection = new ContactDB().makeConnection();
        preparedStatement = connection.prepareStatement(sql);
        resultSet = preparedStatement.executeQuery();
        while (resultSet.next()){
            doctorArrayList.add(new Doctor(
                    resultSet.getInt("id"),
                    resultSet.getString("name"),
                    resultSet.getString("email"),
                    resultSet.getString("degree"),
                    resultSet.getInt("experience"),
                    resultSet.getString("speciality_name"),
                    resultSet.getString("image"),
                    resultSet.getString("phone"),
                    resultSet.getString("dob"),
                    resultSet.getBoolean("gender"),
                    resultSet.getString("address")));
        }
        return doctorArrayList;
    }
    public boolean deleteDoctor(int id){
        String sql = "DELETE FROM `doctors` WHERE id = ?";
        try {
            connection = ContactDB.makeConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            preparedStatement.execute();
            return true;
        } catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

}
