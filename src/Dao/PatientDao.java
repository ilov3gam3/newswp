package Dao;

import Contact.ContactDB;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class PatientDao {
    Connection connection = null;
    PreparedStatement preparedStatement = null;
    ResultSet resultSet = null;
    public boolean createUser(String name, String email, String password, String phone, String verify_key, String address, String dob, boolean gender) throws ClassNotFoundException {
        String sql = "insert into patients(name, email, password, phone, dob, gender, address, is_admin, verify_key, is_verified) values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        try {
            this.connection = ContactDB.makeConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, name);
            preparedStatement.setString(2, email);
            preparedStatement.setString(3, password);
            preparedStatement.setString(4, phone);
            preparedStatement.setString(5, dob);
            preparedStatement.setBoolean(6, gender);
            preparedStatement.setString(7, address);
            preparedStatement.setBoolean(8, false);
            preparedStatement.setString(9, verify_key);
            preparedStatement.setBoolean(10, false);
            preparedStatement.execute();
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
        return true;
    }
    public boolean emailExist(String email) throws SQLException, ClassNotFoundException {
        String sql = "select count(id) from patients where email = ?";
        this.connection = ContactDB.makeConnection();
        preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setString(1, email);
        resultSet = preparedStatement.executeQuery();
        while (resultSet.next()){
            int status = resultSet.getInt(1);
            if (status == 0) {
                return false;
            } else {
                return true;
            }
        }
        return false;
    }
}
