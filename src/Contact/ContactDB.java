package Contact;

import jakarta.servlet.ServletContext;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
public class ContactDB {
    public static Connection makeConnection2() throws ClassNotFoundException, SQLException {
        String jdbcUrl = "jdbc:" + System.getProperty("DB_TYPE") + "://" + System.getProperty("DB_HOST") + ":" + System.getProperty("DB_PORT") + "/" + System.getProperty("DB_NAME");
        if (System.getProperty("DB_TYPE").equals("mysql")){
            Class.forName("com.mysql.cj.jdbc.Driver");
        } else if (System.getProperty("DB_TYPE").equals("sqlserver")) {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        }
        return (Connection) DriverManager.getConnection(jdbcUrl, System.getProperty("DB_USERNAME"), System.getProperty("DB_PASSWORD"));
    }
    public static Connection makeConnection() throws ClassNotFoundException, SQLException {
        /*int choice = 1; // 0 sqlserver || 1 mysql
        if(choice == 0 ){
            try {
                String serverName = "LAPTOP-JG837OIA";
                String databaseName = "SWP391";
                String url = "jdbc:sqlserver://" + serverName + ";databaseName=" + databaseName + ";encrypt=false";
                String username = "sa";
                String password = "sa";
                Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
                return (Connection) DriverManager.getConnection(url, username, password);
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        } else {
            String jdbcUrl = "jdbc:mysql://localhost:3306/swp";
            String username = "root";
            String password = "";
            *//*String jdbcUrl = "jdbc:mysql://win-replacing.at.ply.gg:23018/swp";
            String username = "minh";
            String password = "";*//*
            try {
                Class.forName("com.mysql.cj.jdbc.Driver");
                // Connection established successfully
                return DriverManager.getConnection(jdbcUrl, username, password);
            } catch (ClassNotFoundException e) {
                // Handle driver loading error
                e.printStackTrace();
                return null;
            } catch (SQLException e) {
                // Handle connection errors
                e.printStackTrace();
                return null;
            }
        }*/

        String jdbcUrl = "jdbc:" + System.getProperty("DB_TYPE") + "://" + System.getProperty("DB_HOST") + ":" + System.getProperty("DB_PORT") + "/" + System.getProperty("DB_NAME");
        if (System.getProperty("DB_TYPE").equals("mysql")){
            Class.forName("com.mysql.cj.jdbc.Driver");
        } else if (System.getProperty("DB_TYPE").equals("sqlserver")) {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        }
        return (Connection) DriverManager.getConnection(jdbcUrl, System.getProperty("DB_USERNAME"), System.getProperty("DB_PASSWORD"));
    }
    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        MyServletContextListener.loadEnvVariables();
        /*try {
            Connection connection = ContactDB.makeConnection();
            if (connection != null) {
                System.out.println("Kết nối thành công!");
                connection.close();
            } else {
                System.out.println("Kết nối thất bại!");
            }
        } catch (ClassNotFoundException e) {
            System.out.println("Không tìm thấy driver!");
        } catch (SQLException e) {
            System.out.println("Lỗi khi kết nối cơ sở dữ liệu: " + e.getMessage());
        }*/
        System.out.println(makeConnection2().getCatalog());
    }
}
