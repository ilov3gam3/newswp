package Contact;

import jakarta.servlet.ServletContextEvent;
import jakarta.servlet.ServletContextListener;

public class MyServletContextListener implements ServletContextListener {
    public static void loadEnvVariables() {
        System.setProperty("DB_TYPE", "mysql"); // có thể thay bằng sqlserver
        System.setProperty("DB_HOST", "localhost");
        System.setProperty("DB_PORT", "3306");
        System.setProperty("DB_NAME", "swp");
        System.setProperty("DB_USERNAME", "root");
        System.setProperty("DB_PASSWORD", "");
        System.setProperty("APP_NAME", "newswp");
    }
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        loadEnvVariables();
    }
}
