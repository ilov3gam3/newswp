package Control;

import Dao.DoctorScheduleDao;
import Model.Doctor;
import Model.DoctorSchedule;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.time.temporal.WeekFields;
import java.util.ArrayList;
import java.util.Locale;

public class DoctorScheduleServlet extends HttpServlet {
    public String[] getDateOfWeek(int year, int week) {
        String[] dateArray = new String[8];
        LocalDate firstDayOfWeek = LocalDate.ofYearDay(year, 1).with(DayOfWeek.MONDAY).plusWeeks(week);
        for (int i = 1; i < 8; i++) {
            LocalDate date = firstDayOfWeek.plusDays(i - 1);
            String formattedDate = date.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
            dateArray[i] = formattedDate;
        }
        dateArray[0] = "null";
        return dateArray;
    }


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int year,weekNumber;
        if (req.getSession().getAttribute("week_and_year_url") != null){
            String current_week = (String) req.getSession().getAttribute("week_and_year_url");
            String[] arr = current_week.split("-W");
            year = Integer.parseInt(arr[0]);
            weekNumber = Integer.parseInt(arr[1]);
            req.getSession().removeAttribute("week_and_year_url");
        } else {
            LocalDate currentDate = LocalDate.now();
            WeekFields weekFields = WeekFields.of(Locale.getDefault());
            weekNumber = currentDate.get(weekFields.weekOfWeekBasedYear());
            year = currentDate.get(weekFields.weekBasedYear());
        }
        String[] dateArray = getDateOfWeek(year, weekNumber);
        String current_week = year + "-W" + weekNumber;
        if (req.getSession().getAttribute("url_mess") != null) {
            String url_mess = (String) req.getSession().getAttribute("url_mess");
            req.setAttribute(url_mess.split("\\|")[0], url_mess.split("\\|")[1]);
            req.getSession().removeAttribute("url_mess");
        }
        String first_date_week = dateArray[1];
        String last_date_week = dateArray[7];
        int doctor_id = ((Doctor) req.getSession().getAttribute("doctor")).getId();
        DoctorScheduleDao doctorScheduleDao = new DoctorScheduleDao();
        ArrayList<DoctorSchedule> doctorScheduleArrayList = doctorScheduleDao.getScheduleOfWeek(doctor_id, first_date_week, last_date_week);
        String[][] table = new String[11][8];
        table[0] = new String[]{"Time", "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
        table[1] = dateArray;
        for (int i = 2; i < 11; i++) {
            table[i][0] = (i + 6) + "h -> " + (i + 7) + "h";
        }
        for (int i = 2; i < 11; i++) {
            for (int j = 1; j < 8; j++) {
                int table_date = Integer.parseInt(table[1][j].split("-")[2]);
                for (int k = 0; k < doctorScheduleArrayList.size(); k++) {
                    int start_time = Integer.parseInt(doctorScheduleArrayList.get(k).start.split(" ")[1].split(":")[0]);
                    int end_time = Integer.parseInt(doctorScheduleArrayList.get(k).end.split(" ")[1].split(":")[0]);
                    int date = Integer.parseInt(doctorScheduleArrayList.get(k).start.split(" ")[0].split("-")[2]);
                    if (table_date == date){
                        if ((i + 6) >= start_time && (i + 7) <= end_time) {
                            table[i][j] = "bận";
                        }
                    }

                }
            }
        }
        req.setAttribute("current_week", current_week);
        req.setAttribute("table", table);
        req.getRequestDispatcher("/WEB-INF/views/doctor/schedule.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getParameter("_method").equals("get_date")) {
            String week_and_year = req.getParameter("week");
            req.getSession().setAttribute("week_and_year_url", week_and_year);
            resp.sendRedirect(req.getContextPath() + "/doctor/schedule");
        } else if (req.getParameter("_method").equals("add_schedule")) {
            String date = req.getParameter("date");
            int start = Integer.parseInt(req.getParameter("from"));
            int end = Integer.parseInt(req.getParameter("to"));
            int doctor_id = ((Doctor) req.getSession().getAttribute("doctor")).getId();
            if (end < start || start < 8 || end > 17) {
                req.getSession().setAttribute("url_mess", "error|Thời gian kết thúc phải nhỏ hơn thời gian bắt đầu, thời gian nằm trong khoảng từ 8h đến 17h!");
                resp.sendRedirect(req.getContextPath() + "/doctor/schedule");
            } else {
                DoctorScheduleDao doctorScheduleDao = new DoctorScheduleDao();
                if (doctorScheduleDao.addSchedule(doctor_id, date, start, end)) {
                    req.getSession().setAttribute("url_mess", "success|Thêm thành công!");
                    resp.sendRedirect(req.getContextPath() + "/doctor/schedule");
                } else {
                    req.getSession().setAttribute("url_mess", "error|Đã có lỗi!");
                    resp.sendRedirect(req.getContextPath() + "/doctor/schedule");
                }
            }
        }
    }
}
