package Control;

import Dao.AuthDao;
import Model.Doctor;
import Model.User;
import jakarta.servlet.http.*;
import jakarta.servlet.*;

import java.io.IOException;





public class LoginServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("show_login", 1);
        System.out.println(request.getAttribute("from_filter"));
//        Boolean.parseBoolean(request.getAttribute("from_filter").) == true ? request.setAttribute("login_mess", "Vui lòng đăng nhập!") :
        request.getRequestDispatcher("/WEB-INF/views/auth/login.jsp").forward(request, response);
    }
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("vao do post login");
        int choice = Integer.parseInt(request.getParameter("select"));
        System.out.println(choice);
        System.out.println(request.getParameter("select"));
        if (choice == 0){
            String email = request.getParameter("email");
            String password = request.getParameter("password");
            System.out.println(email + "-" + password);
            AuthDao dao = new AuthDao();
            User user = dao.checkLoginUser(email, password);
            if (user != null){
                request.getSession().setAttribute("acc", user);
                request.getSession().setAttribute("login", true);
                if (user.is_admin()){
                    request.getSession().setAttribute("admin", true);
                }
                response.sendRedirect("home");
            } else {
                request.setAttribute("login_mess", "Sai email hoặc mật khẩu!");
                request.setAttribute("show_login", 1);
                request.getRequestDispatcher("/WEB-INF/views/auth/login.jsp").forward(request, response);
            }
        } else if (choice == 1){
            String email = request.getParameter("email");
            String password = request.getParameter("password");
            System.out.println(email + "-" + password);
            AuthDao dao = new AuthDao();
            Doctor doctor = dao.checkLoginDoctor(email, password);
            if (doctor != null){
                request.getSession().setAttribute("doctor", doctor);
                request.getSession().setAttribute("login", true);
                response.sendRedirect("home");
            } else {
                request.setAttribute("login_mess", "Sai email hoặc mật khẩu!");
                request.setAttribute("show_login", 1);
                request.getRequestDispatcher("/WEB-INF/views/auth/login.jsp").forward(request, response);
            }
        }
    }
}
