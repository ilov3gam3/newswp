package Model;

public class DoctorSchedule {
    public int id;
    public int doctor_id;
    public String start;
    public String end;

    public DoctorSchedule(int id, int doctor_id, String start, String end) {
        this.id = id;
        this.doctor_id = doctor_id;
        this.start = start;
        this.end = end;
    }

    @Override
    public String toString() {
        return "DoctorSchedule{" +
                "id=" + id +
                ", doctor_id=" + doctor_id +
                ", start='" + start + '\'' +
                ", end='" + end + '\'' +
                '}';
    }
}
